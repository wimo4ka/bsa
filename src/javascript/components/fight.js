import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over

    		// playerOne, playerTwo - исходные данные бойцов		
		const playerOne = firstFighter;
		const playerTwo = secondFighter;
		
		// playerOneFights, playerTwoFights - данные бойцов	во время боя	
		let playerOneFights = Object.assign({}, playerOne);
		let playerTwoFights = Object.assign({}, playerTwo);
		
		// обнуляем показатели защиты, они будут присваиваться только если игрок нажал кнопку блока
		playerOneFights.defense = 0;
		playerTwoFights.defense = 0;
		
		// исходное здоровье
		const playerOneInitialHealth = playerOne.health; 
		const playerTwoInitialHealth = playerTwo.health;
		
		// здоровье во время боя
		let playerOneHealth = playerOneInitialHealth; 
		let playerTwoHealth = playerTwoInitialHealth;
		
		let bufferOne = []; // буффер нажатых одновременно клавиш первого игрока
		let bufferTwo = []; // буффер нажатых одновременно клавиш второго игрока
		let lastKeyTimeOne = Date.now(); // время последнего нажатия первого игрока
		let lastKeyTimeTwo = Date.now(); // время последнего нажатия второго игрока		
		
		document.addEventListener("keyup", function(event) {
			
			const playerOneHealthIndicator = document.getElementById('left-fighter-indicator');
			const playerTwoHealthIndicator = document.getElementById('right-fighter-indicator');
		
			// first fighter hits (A key)
			if ('Key' + event.key.toUpperCase() == controls.PlayerOneAttack) {
				handleHit(playerOne, playerTwoFights, playerTwoInitialHealth, playerTwoHealthIndicator, resolve);
			}
			// second fighter hits (J key)
			if ('Key' + event.key.toUpperCase() == controls.PlayerTwoAttack) {
				handleHit(playerTwo, playerOneFights, playerOneInitialHealth, playerOneHealthIndicator, resolve);
			}
			
			// first fighter releases block (D key UP)
			if ('Key' + event.key.toUpperCase() == controls.PlayerOneBlock) {
				playerOneFights.defense = 0;
			}
			// second fighter releases block (L key UP)
			if ('Key' + event.key.toUpperCase() == controls.PlayerTwoBlock) {
				playerTwoFights.defense = 0;
			}
			
			// combos
			
			const key = event.key;		
			const currentTime = Date.now();			
			
			if (currentTime - lastKeyTimeOne > 10000) { // если прошло больше 10 секунд после последнего комбо первого игрока
			
				bufferOne.push('Key' + key.toUpperCase());
				
				bufferOne = [...new Set(bufferOne)]; // уникализируем буфер	
				
				if (bufferOne.length > 3) bufferOne = [];
				
				// комбо первого игрока
				if(isEqual(bufferOne.sort(), controls.PlayerOneCriticalHitCombination.sort())) {
					bufferOne = [];
					lastKeyTimeOne = currentTime;
					console.log('комбо первого игрока');
					handleHit(playerOne, playerTwoFights, playerTwoInitialHealth, playerTwoHealthIndicator, resolve, true);
				}
				
			} else {
				bufferOne = [];
			}
			
			if (currentTime - lastKeyTimeTwo > 10000) { // если прошло больше 10 секунд после последнего комбо второго игрока				
				
				bufferTwo.push('Key' + key.toUpperCase());
				
				bufferTwo = [...new Set(bufferTwo)]; // уникализируем буфер
				
				if (bufferTwo.length > 3) bufferTwo = [];
				
				
				// комбо второго игрока
				if(isEqual(bufferTwo.sort(), controls.PlayerTwoCriticalHitCombination.sort())) {
					bufferTwo = [];
					lastKeyTimeTwo = currentTime;
					console.log('комбо второго игрока');
					handleHit(playerTwo, playerOneFights, playerOneInitialHealth, playerOneHealthIndicator, resolve, true);
				}
			} else {
				bufferTwo = [];
			}
			
		});
		
		document.addEventListener("keydown", function(event) {
			
			// first fighter blocks (D key)
			if ('Key' + event.key.toUpperCase() == controls.PlayerOneBlock) {
				handleBlock(playerOne, playerOneFights);
			}
			// second fighter blocks (L key)
			if ('Key' + event.key.toUpperCase() == controls.PlayerOneBlock) {
				handleBlock(playerTwo, playerTwoFights);
			}
		});	
	});
}


export function getDamage(attacker, defender) {
  // return damage
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage < 0 ? 0 : damage;
}

export function getHitPower(fighter, isCombo = false) {
  // return hit power
  let criticalHitChance = Math.random() + 1;
  if (isCombo) criticalHitChance = 2;
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  // return block power
  const dodgeChance = Math.random() + 1;
  return fighter.defense * dodgeChance;
}

// compare arrays (need to sort first)
function isEqual (first, second) {
	return first.length === second.length && 
		first.every((value, index) => value === second[index]);
}

export function handleHit(attacker, defenderFights, defenderInitialHealth, defenderHealthIndicator, resolve, isCombo = false) {
	
	defenderFights.health -= getDamage(attacker, defenderFights, isCombo);
	
	defenderHealthIndicator.style.width = (defenderFights.health * 100 / defenderInitialHealth) + '%';
	
	if (defenderFights.health <= 0) {
		defenderHealthIndicator.style.width = '0%';
		resolve(attacker);
	}	
}
export function handleBlock(defender, defenderFights) {
	defenderFights.defense = getBlockPower(defender);	
}

