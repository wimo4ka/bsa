import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)
  const details = createElement({
    tagName: 'ul',
    className: 'fighter-details'
  })
  if (fighter) {
    for (let key in fighter) {
      if (key !== '_id' && key !== 'source') {
        const row = createElement({
          tagName: 'li',
          className: 'fighter-details-item'
        })
        row.innerHTML = `${key} : ${fighter[key]}`
        details.append(row)
      } else if (key === 'source') {
        const row = createFighterImage(fighter);
        row.classList.add('short-size');
        fighterElement.append(row)
      }
    }
    fighterElement.prepend(details)
  }
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
