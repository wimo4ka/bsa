import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';
import App from '../../app';

export function showWinnerModal(fighter) {
  // call showModal function 
  const bodyElement = createElement({ tagName: 'div' });
  const onClose = () => {
    App.rootElement.innerHTML = '';
    return new App();
  };
  showModal({ title: `${fighter.name} won`, bodyElement, onClose });
}
